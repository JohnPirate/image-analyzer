@extends('layouts.default-container')

@push('page-styles')
@endpush

@section('container-content')
    <div class="mb-3">
        <h4>Document {{ $doc->id }}</h4>
    </div>

    <div class="mb-3">
        <a href="{{ route('documents.edit', ['documentId' => $doc->id]) }}"
           class="btn btn-default"
        >
            <i class="far fa-edit"></i><span class="ml-2">Edit</span>
        </a>
        <a href="{{ route('documents.editor', ['documentId' => $doc->id]) }}"
           class="btn btn-default"
        >
            <i class="fas fa-tools"></i><span class="ml-2">Feature Editor</span>
        </a>
        @include('components.delete-document', ['documentId'=>$doc->id])
    </div>

    <div class="mb-3">
        <div class="card">
            <div class="card-body">
                <h5 class="m-0">Meta</h5>
            </div>
            @if(isset($doc->meta) && $doc->meta)
                <table class="table mb-0">
                    <tbody>
                    @foreach($doc->meta as $key => $val)
                        <tr>
                            <td><strong>{{ $key }}</strong></td>
                            <td>{{ $val }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="card-body text-center p-5 border-top">
                    <h6>No MetaData Found</h6>
                </div>
            @endif
        </div>
    </div>

    <div class="mb-3">
        <div class="card" id="imageContainer">
            <div class="card-body">
                <h5 class="m-0">Images</h5>
            </div>
            @if(count($doc->imageFiles) > 0)
                @foreach($doc->imageFiles as $imageFile)
                    <div class="media border-top p-2 imgData" data-img-file-id="{{$imageFile->id}}">
                        <div class="mr-2 product-image" style="background-image: url('{{ Storage::url('images/'.$imageFile->name) }}?v={{ $imageFile->created_at }}');"></div>
                        <div class="media-body">
                            <div class="mb-2">
                                <p class="m-0">Name: {{$imageFile->name}}</p>
                                <p class="m-0">Type: <span class="badge badge-secondary">{{$imageFile->type}}</span></p>
                            </div>
                            <select name="typeSelect" class="imgTypeSelect form-control form-control-sm mb-2" value="{{ $imageFile->type }}">
                                @foreach(config('common.images.types') as $type)
                                    @if($type === $imageFile->type)
                                        <option value="{{ $type }}" selected>{{ __('images.types.'.$type) }}</option>
                                    @else
                                        <option value="{{ $type }}">{{ __('images.types.'.$type) }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @include('components.delete-image', ['btnSize' => 'sm', 'imageId' => $imageFile->id, 'formId' => 'deleteAction_'.$imageFile->id])
                        </div>
                    </div>
                @endforeach
            @else
                <div class="card-body text-center p-5 border-top">
                    <h6>No Images Found</h6>
                </div>
            @endif
            <div class="card-body border-top">
                <h5>Image Upload</h5>
                <form method="POST"
                      action="{{ route('images.upload') }}"
                      enctype="multipart/form-data"
                >
                    @method('POST')
                    @csrf
                    @include('components.image-upload-fields')
                    <input type="hidden" name="document_id" value="{{ $doc->id }}">
                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('page-scripts')
<script>

    function updateImageType(id, type) {
        return axios.patch(`/images/${id}`, {type});
    }

    document.addEventListener('DOMContentLoaded', () => {
        setEventListener(document.getElementsByClassName('deleteDocumentAction'), 'submit', deleteDocumentModal);
        setEventListener(document.getElementsByClassName('deleteImageAction'), 'submit', deleteImageModal);
    });

    $('#imageContainer').on('change', '.imgTypeSelect', function (e) {
        e.preventDefault();
        let rootParent = $(this).parent().parent();
        let data = rootParent.data();

        const imgFileId = data.imgFileId;
        const imgFileType = this.value;

        console.log('action, imgTypeSelect',imgFileId, imgFileType);

        updateImageType(imgFileId, imgFileType)
            .then((response) =>{
                console.log(response.data.message);
                rootParent.find('span.badge.badge-secondary').text(imgFileType);
            })
            .catch((error) =>{
                console.log(error.response);
                rootParent.addClass('bg-danger');
            })
        ;
    });
</script>
@endpush
