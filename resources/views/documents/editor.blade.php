@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('components.navbar')
                    <div class="mb-3">
                        <h4>Editor {{ $doc->id }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="image-editor"
         data-doc-id="{{ $doc->id }}"
    ></div>



@endsection

@push('page-scripts')
<script src="{{ mix('js/image_editor.js') }}"></script>
@endpush
