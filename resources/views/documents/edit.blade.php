@extends('layouts.default-container')

@push('page-styles')

@endpush

@section('container-content')
    <div class="mb-3">
        <h4>Edit Document {{ $doc->id }}</h4>
    </div>

    <div class="mb-3">
        <a href="{{ route('documents.show', ['documentId' => $doc->id]) }}"
           class="btn btn-default"
        >
            Cancel
        </a>
    </div>

    <div class="mb-3">
        <div class="card">
            <div class="card-body">
                <h5>Images</h5>
            </div>
            <div class="hslide">
                <div v-show="hasScrollHint" class="hslide__scroll-hint-right"></div>
                <div class="hslide__scroll-container">
                    <div class="hslide__list">
                        @foreach($doc->imageFiles as $imageFile)
                            <img src="{{ Storage::url('images/'.$imageFile->name) }}" class="img-thumbnail">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-3">
        <div class="card">
            <div class="card-body">
                <h5>MetaData</h5>
                <div id="meta-form"
                     data-doc-id="{{ $doc->id }}"
                     data-form='@json($doc->meta)'
                ></div>
            </div>
        </div>
    </div>

@endsection

@push('page-scripts')
<script src="{{ mix('js/meta_form.js') }}"></script>
@endpush
