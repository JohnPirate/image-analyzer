@extends('layouts.default-container')

@push('page-styles')
@endpush

@section('container-content')
    <div class="mb-3">
        <h4>Overview Documents</h4>
    </div>

    @if(count($documents) > 0)
        @foreach($documents as $doc)
            <div class="card mb-3">
                <div class="card-body p-2">
                    <div class="media">
                        @if(count($doc->imageFiles) > 0)
                            <div class="mr-2 product-image img-thumbnail" style="background-image: url('{{ Storage::url('images/'.$doc->imageFiles->first()->name) }}?v={{$doc->imageFiles->first()->created_at}}');"></div>
                        @else
                            <div class="mr-2 product-image img-thumbnail bg-default"></div>
                        @endif
                        <div class="media-body">
                            <div>
                                <a href="{{ route('documents.show', ['documentId' => $doc->id]) }}">
                                    {{ $doc->id }}
                                </a>
                            </div>
                            <div class="text-muted mb-2">
                                <small>Images: {{ count($doc->imageFiles) }}</small>
                                @foreach($doc->imageFiles as $imageFile)
                                    <span class="badge badge-secondary">{{ $imageFile->type }}</span>
                                @endforeach
                            </div>
                            <div>
                                <a href="{{ route('documents.edit', ['documentId' => $doc->id]) }}"
                                   class="btn btn-default btn-sm"
                                >
                                    <i class="far fa-edit"></i><span class="ml-2">Edit</span>
                                </a>
                                <a href="{{ route('documents.editor', ['documentId' => $doc->id]) }}"
                                   class="btn btn-default btn-sm"
                                >
                                    <i class="fas fa-tools"></i><span class="ml-2">Feature Editor</span>
                                </a>
                                @include('components.delete-document', ['btnSize' => 'sm', 'documentId' => $doc->id, 'formId' => 'deleteAction_'.$doc->id])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="card">
            <div class="card-body m-5 text-center">
                <h5>No Documents Found</h5>
            </div>
        </div>
    @endif

@endsection

@push('page-scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            setEventListener(document.getElementsByClassName('deleteDocumentAction'), 'submit', deleteDocumentModal);
        });
    </script>
@endpush
