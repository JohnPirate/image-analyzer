@extends('layouts.default-container')

@push('page-styles')
@endpush

@section('container-content')
    <div class="mb-3">
        <h4>Create new image document</h4>
    </div>
    <form method="POST"
          action="{{ route('documents.create') }}"
          enctype="multipart/form-data"
      >
        @method('POST')
        @csrf
        @include('components.image-upload-fields')
        <button type="submit" class="btn btn-primary">Create new document</button>
    </form>
@endsection

@push('page-scripts')
@endpush
