@extends('layouts.app')

@section('page-content')
    <div class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('components.navbar')
                    @includeWhen(session()->has('alert_msg'), 'components.alert')
                    @yield('container-content')
                </div>
            </div>
        </div>
    </div>
@endsection
