<?php
$btnSize = $btnSize ?? 'md';
$formId = $formId ?? 'actionDeleteImage';
?>
<form class="m-0 d-inline-block deleteImageAction"
      method="POST"
      id="{{ $formId }}"
      action="{{ route('images.delete', ['imageId' => $imageId]) }}"
>
    @method('DELETE')
    @csrf
    <button type="submit"
            class="btn btn-danger btn-{{ $btnSize }}"
    >
        <i class="fas fa-trash-alt"></i><span class="ml-2">Delete</span>
    </button>
</form>
