<?php
$btnSize = $btnSize ?? 'md';
$formId = $formId ?? 'actionDeleteDocument';
?>
<form class="m-0 d-inline-block deleteDocumentAction"
      method="POST"
      id="{{ $formId }}"
      action="{{ route('documents.delete', ['documentId' => $documentId]) }}"
>
    @method('DELETE')
    @csrf
    <button type="submit"
            class="btn btn-danger btn-{{ $btnSize }}"
    >
        <i class="fas fa-trash-alt"></i><span class="ml-2">Delete</span>
    </button>
</form>
