

<div class="form-group" >
    <label for="formImageUpload">Images</label>
    <input type="file"
           name="images[]"
           class="form-control @error('images') is-invalid @enderror"
           id="formImageUpload"
           accept="image/*"
           multiple="multiple"
    >
    @error('images')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
    <small id="formImageUploadHelp" class="form-text text-muted">Image size: -</small>
</div>

<div class="form-group">
    <label for="formImageType">Select Type</label>
    <select class="form-control @error('type') is-invalid @enderror" id="formImageType" name="type">
        @foreach(config('common.images.types') as $type)
            <option value="{{ $type }}">{{ __('images.types.'.$type) }}</option>
        @endforeach
    </select>
    @error('type')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

@push('page-scripts')
<script>
    document.addEventListener('DOMContentLoaded', () => {

        const fileField = document.getElementById('formImageUpload');
        const fileFieldHelp = document.getElementById('formImageUploadHelp');

        fileField.addEventListener('change', () => {
            if (fileField.files.length > 0) {
                fileFieldHelp.innerText = '';
                let fileSizeTotal = 0;
                for (let idx = 0; idx < fileField.files.length; idx++) {
                    let file = fileField.files[idx];
                    fileSizeTotal += file.size;
                    let fileSize = humanFileSize(round(file.size, 3));
                    fileFieldHelp.innerText += `Image size: ${fileSize}\n`;
                }
                fileFieldHelp.innerText += `Total size: ${humanFileSize(round(fileSizeTotal, 3))}`;
            } else {
                fileFieldHelp.innerText = `Image size: -`;
            }
        });

    });
</script>
@endpush
