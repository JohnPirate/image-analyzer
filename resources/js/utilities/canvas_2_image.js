/**
 * covert canvas to image
 * and save the image file
 */

const downloadMime = 'image/octet-stream';

export default class Canvas2Image {

    // check if support sth.
    static $support() {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        return {
            canvas: !!ctx,
            imageData: !!ctx.getImageData,
            dataURL: !!canvas.toDataURL,
            btoa: !!window.btoa
        };
    }

    static scaleCanvas (canvas, width, height) {
        const w = canvas.width;
        const h = canvas.height;

        if (width === undefined) {
            width = w;
        }
        if (height === undefined) {
            height = h;
        }

        const retCanvas = document.createElement('canvas');
        const retCtx = retCanvas.getContext('2d');
        retCanvas.width = width;
        retCanvas.height = height;
        retCtx.drawImage(canvas, 0, 0, w, h, 0, 0, width, height);
        return retCanvas;
    }

    static getDataURL (canvas, type, width, height) {
        canvas = Canvas2Image.scaleCanvas(canvas, width, height);
        return canvas.toDataURL(type);
    }

    static saveFile (strData,filename) {
        const save_link = document.createElement('a');
        save_link.href = strData;
        save_link.download = filename;
        const event = new MouseEvent('click',{"bubbles":false, "cancelable":false});
        save_link.dispatchEvent(event);

    }

    static genImage(strData) {
        const img = document.createElement('img');
        img.src = strData;
        return img;
    }

    static fixType (type) {
        type = type.toLowerCase().replace(/jpg/i, 'jpeg');
        const r = type.match(/png|jpeg|bmp|gif/)[0];
        return 'image/' + r;
    }

    static encodeData (data) {
        if (!window.btoa) { throw 'btoa undefined' }
        let str = '';
        if (typeof data === 'string') {
            str = data;
        } else {
            for (let i = 0; i < data.length; i ++) {
                str += String.fromCharCode(data[i]);
            }
        }

        return btoa(str);
    }

    static getImageData (canvas) {
        const w = canvas.width,
            h = canvas.height;
        return canvas.getContext('2d').getImageData(0, 0, w, h);
    }

    static makeURI (strData, type) {
        return 'data:' + type + ';base64,' + strData;
    }


    /**
     * create bitmap image
     */
    static genBitmapImage (oData) {

        //
        // BITMAPFILEHEADER: http://msdn.microsoft.com/en-us/library/windows/desktop/dd183374(v=vs.85).aspx
        // BITMAPINFOHEADER: http://msdn.microsoft.com/en-us/library/dd183376.aspx
        //

        const biWidth = oData.width;
        const biHeight = oData.height;
        const biSizeImage = biWidth * biHeight * 3;
        const bfSize = biSizeImage + 54; // total header size = 54 bytes

        //
        //  typedef struct tagBITMAPFILEHEADER {
        //  	WORD bfType;
        //  	DWORD bfSize;
        //  	WORD bfReserved1;
        //  	WORD bfReserved2;
        //  	DWORD bfOffBits;
        //  } BITMAPFILEHEADER;
        //
        const BITMAPFILEHEADER = [
            // WORD bfType -- The file type signature; must be "BM"
            0x42, 0x4D,
            // DWORD bfSize -- The size, in bytes, of the bitmap file
            bfSize & 0xff, bfSize >> 8 & 0xff, bfSize >> 16 & 0xff, bfSize >> 24 & 0xff,
            // WORD bfReserved1 -- Reserved; must be zero
            0, 0,
            // WORD bfReserved2 -- Reserved; must be zero
            0, 0,
            // DWORD bfOffBits -- The offset, in bytes, from the beginning of the BITMAPFILEHEADER structure to the bitmap bits.
            54, 0, 0, 0,
        ];

        //
        //  typedef struct tagBITMAPINFOHEADER {
        //  	DWORD biSize;
        //  	LONG  biWidth;
        //  	LONG  biHeight;
        //  	WORD  biPlanes;
        //  	WORD  biBitCount;
        //  	DWORD biCompression;
        //  	DWORD biSizeImage;
        //  	LONG  biXPelsPerMeter;
        //  	LONG  biYPelsPerMeter;
        //  	DWORD biClrUsed;
        //  	DWORD biClrImportant;
        //  } BITMAPINFOHEADER, *PBITMAPINFOHEADER;
        //
        const BITMAPINFOHEADER = [
            // DWORD biSize -- The number of bytes required by the structure
            40, 0, 0, 0,
            // LONG biWidth -- The width of the bitmap, in pixels
            biWidth & 0xff, biWidth >> 8 & 0xff, biWidth >> 16 & 0xff, biWidth >> 24 & 0xff,
            // LONG biHeight -- The height of the bitmap, in pixels
            biHeight & 0xff, biHeight >> 8 & 0xff, biHeight >> 16 & 0xff, biHeight >> 24 & 0xff,
            // WORD biPlanes -- The number of planes for the target device. This value must be set to 1
            1, 0,
            // WORD biBitCount -- The number of bits-per-pixel, 24 bits-per-pixel -- the bitmap
            // has a maximum of 2^24 colors (16777216, Truecolor)
            24, 0,
            // DWORD biCompression -- The type of compression, BI_RGB (code 0) -- uncompressed
            0, 0, 0, 0,
            // DWORD biSizeImage -- The size, in bytes, of the image. This may be set to zero for BI_RGB bitmaps
            biSizeImage & 0xff, biSizeImage >> 8 & 0xff, biSizeImage >> 16 & 0xff, biSizeImage >> 24 & 0xff,
            // LONG biXPelsPerMeter, unused
            0, 0, 0, 0,
            // LONG biYPelsPerMeter, unused
            0, 0, 0, 0,
            // DWORD biClrUsed, the number of color indexes of palette, unused
            0, 0, 0, 0,
            // DWORD biClrImportant, unused
            0, 0, 0, 0,
        ];

        const iPadding = (4 - ((biWidth * 3) % 4)) % 4;

        const aImgData = oData.data;

        let strPixelData = '';
        const biWidth4 = biWidth << 2;
        let y = biHeight;
        const fromCharCode = String.fromCharCode;

        do {
            let iOffsetY = biWidth4 * (y - 1);
            let strPixelRow = '';
            for (let x = 0; x < biWidth; x++) {
                let iOffsetX = x << 2;
                strPixelRow += fromCharCode(aImgData[iOffsetY+iOffsetX+2]) +
                    fromCharCode(aImgData[iOffsetY+iOffsetX+1]) +
                    fromCharCode(aImgData[iOffsetY+iOffsetX]);
            }

            for (let c = 0; c < iPadding; c++) {
                strPixelRow += String.fromCharCode(0);
            }

            strPixelData += strPixelRow;
        } while (--y);

        return Canvas2Image.encodeData(BITMAPFILEHEADER.concat(BITMAPINFOHEADER)) + Canvas2Image.encodeData(strPixelData);
    };


    /**
     * [saveAsImage]
     * @param  {HTMLElement} canvas   [canvasElement]
     * @param  {Number} width    [optional] png width
     * @param  {Number} height   [optional] png height
     * @param  {string} type     [image type]
     * @param  {String} filename [image filename]
     * @return {type}          [description]
     */
    static saveAsImage (canvas, width, height, type,filename) {
        let strData;
        if (Canvas2Image.$support().canvas && Canvas2Image.$support().dataURL) {
            if (typeof canvas == "string") {
                canvas = document.getElementById(canvas);
            }
            if (type === undefined) {
                type = 'png';
            }
            filename = filename === undefined
                        || filename.length === 0 ? Date.now()+'.'+type : filename+'.'+type;
            type = Canvas2Image.fixType(type);

            if (/bmp/.test(type)) {
                const data = Canvas2Image.getImageData(Canvas2Image.scaleCanvas(canvas, width, height));
                strData = Canvas2Image.genBitmapImage(data);
                Canvas2Image.saveFile(Canvas2Image.makeURI(strData, downloadMime),filename);
            } else {
                strData = Canvas2Image.getDataURL(canvas, type, width, height);
                Canvas2Image.saveFile(strData.replace(type, downloadMime),filename);
            }
        }
    }

    static convertToImage(canvas, width, height, type) {
        let strData;
        if (Canvas2Image.$support().canvas && Canvas2Image.$support().dataURL) {
            if (typeof canvas == "string") {
                canvas = document.getElementById(canvas);
            }
            if (type === undefined) {
                type = 'png';
            }
            type = Canvas2Image.fixType(type);
            if (/bmp/.test(type)) {
                const data = Canvas2Image.getImageData(Canvas2Image.scaleCanvas(canvas, width, height));
                strData = Canvas2Image.genBitmapImage(data);
                return Canvas2Image.genImage(Canvas2Image.makeURI(strData, 'image/bmp'));
            } else {
                strData = Canvas2Image.getDataURL(canvas, type, width, height);
                return Canvas2Image.genImage(strData);
            }
        }
    }

    static convertToImageFile(canvas, width, height, type, name) {
        type = Canvas2Image.fixType(type);
        const dataURL = Canvas2Image.getDataURL(canvas, type, width, height);
        return new Promise((resolve) => {
            const blobBin = atob(dataURL.split(',')[1]);
            const array = [];
            for (let i = 0; i < blobBin.length; i += 1) {
                array.push(blobBin.charCodeAt(i));
            }
            resolve(new File([new Uint8Array(array)], name, { type: type }));
        });
    }

    static saveAsPNG(canvas, width, height, fileName) {
        return Canvas2Image.saveAsImage(canvas, width, height, 'png', fileName);
    }

    static saveAsJPEG(canvas, width, height, fileName) {
        return Canvas2Image.saveAsImage(canvas, width, height, 'jpeg', fileName);
    }

    static saveAsGIF(canvas, width, height, fileName) {
        return Canvas2Image.saveAsImage(canvas, width, height, 'gif', fileName);
    }

    static saveAsBMP(canvas, width, height, fileName) {
        return Canvas2Image.saveAsImage(canvas, width, height, 'bmp', fileName);
    }

    static convertToPNG(canvas, width, height) {
        return Canvas2Image.convertToImage(canvas, width, height, 'png');
    }

    static convertToJPEG(canvas, width, height) {
        return Canvas2Image.convertToImage(canvas, width, height, 'jpeg');
    }

    static convertToGIF(canvas, width, height) {
        return Canvas2Image.convertToImage(canvas, width, height, 'gif');
    }

    static convertToBMP(canvas, width, height) {
        return Canvas2Image.convertToImage(canvas, width, height, 'bmp');
    }

    static convertToPNGFile(canvas, width, height, fileName) {
        return Canvas2Image.convertToImageFile(canvas, width, height, 'png', fileName);
    }

    static convertToJPEGFile(canvas, width, height, fileName) {
        return Canvas2Image.convertToImageFile(canvas, width, height, 'jpeg', fileName);
    }

    static convertToGIFFile(canvas, width, height, fileName) {
        return Canvas2Image.convertToImageFile(canvas, width, height, 'gif', fileName);
    }

    static convertToBMPFile(canvas, width, height, fileName) {
        return Canvas2Image.convertToImageFile(canvas, width, height, 'bmp', fileName);
    }
}
