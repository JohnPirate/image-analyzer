/**
 * @type {(v: PropertyKey) => boolean}
 */
export const has = Object.prototype.hasOwnProperty;

/**
 * @param {String} dataURL
 * @param {String} name
 * @param {String} mime
 *
 * @return {Promise<File>}
 */
export function dataUrlToFile(dataURL, name, mime) {
    return new Promise((resolve) => {
        const blobBin = atob(dataURL.split(',')[1]);
        const array = [];
        for (let i = 0; i < blobBin.length; i += 1) {
            array.push(blobBin.charCodeAt(i));
        }
        resolve(new File([new Uint8Array(array)], name, { type: mime }));
    });
}

/**
 * @param {String} dataURL
 * @param {String} name
 *
 * @returns {Blob}
 */
export function dataUrlToBlob(dataURL, name) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURL.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    //Old Code
    //write the ArrayBuffer to a blob, and you're done
    //var bb = new BlobBuilder();
    //bb.append(ab);
    //return bb.getBlob(mimeString);

    //New Code
    return new File([ab], name, {type: mimeString});
    // return new Promise((resolve) => {
    //
    // });
}


/**
 * Calls a function with a minimum of delay
 *
 * @param {Number} refTime
 * @param {Number} minDelay
 * @param {Function} callback
 */
export function minDelay(refTime, minDelay, callback) {
    const diff = Date.now() - refTime;
    if (diff >= minDelay) {
        callback();
        return;
    }
    setTimeout(() => {
        callback();
    }, minDelay - diff);
}
