import {has} from '../../utilities/helpers';

import ImageEditor from '../shared/image_editor';

function resizeEditor() {
    const $editor = $('.tui-image-editor');
    const $container = $('.tui-image-editor-canvas-container');
    const height = parseFloat($container.css('max-height'));
    $editor.height(height);
}


export default {

    /*
    |---------------------------------------------------------------------------
    | Composition
    |---------------------------------------------------------------------------
    */

    // mixins: [],

    // extends: {},

    /*
    |---------------------------------------------------------------------------
    | Assets
    |---------------------------------------------------------------------------
    */

    // directives: {},

    // filters: {},

    components: {
        ImageEditor,
    },

    /*
    |---------------------------------------------------------------------------
    | Lifecycle Hooks
    |---------------------------------------------------------------------------
    */

    // created() {},

    // mounted() {},

    /*
    |---------------------------------------------------------------------------
    | Data
    |---------------------------------------------------------------------------
    */

    // props: {},

    data() {
        return {
            useDefaultUI: false,
            imageEditorOptions: {
                cssMaxWidth: 700,
                cssMaxHeight: 500,
                selectionStyle: {
                    cornerStyle: 'circle',
                    cornerColor: '#eee',
                    cornerStrokeColor: 'blue',
                    cornerSize: 40,
                    lineWidth: 4,
                    borderColor: 'blue',
                    rotatingPointOffset: 100,
                },
                usageStatistics: false,
            },

            drawingMode: null,
            activeImageId: '',
            activeFeatureId: null,

            featureRefPool: {},

            imgPool: {},
            featurePool: {},
            historyPool: {},
        };
    },

    computed: {
        imageEditor() {
            return this.$refs.tuiImageEditor;
        },
        getDrawingMode() {
            return this.drawingMode;
        },
        getActiveImageId() {
            return this.activeImageId;
        },
        hasActiveImage() {
            return !!this.getActiveImageId;
        },
        getActiveFeatureId() {
            return this.activeFeatureId;
        },


        hasFeatureRef() {
            return this.features.length > 0;
        },
        features() {
            return Object.values(this.featureRefPool);
        },
    },

    // watch: {},

    methods: {
        updateInfo() {
            this.drawingMode = this.imageEditor.invoke('getDrawingMode');
            this.activeImageId = this.imageEditor.invoke('getImageName');
        },
        resetAll() {
            this.featureRefPool = {};
            this.activeImageId = '';
            this.stopDrawingMode();
            this.clearRedoStack();
            this.clearUndoStack();
            this.discardSelection();
            return this.clearObjects();
        },


        // Editor Events
        onMouseDown(event, originPointer) {
            // console.log(event);
            // console.log(originPointer);
        },
        onObjectActivated(object) {
            // console.log('onObjectActivated: ', object);
            this.activeFeatureId = object.id;
        },
        onObjectMoved(object) {
            // this.setFeatureRef(object);
        },
        onObjectScaled(object) {
            // this.setFeatureRef(object);
        },
        onObjectRotated(object) {
            // this.setFeatureRef(object);
        },


        // Editor Actions
        cropMode() {
            this.startDrawingMode('CROPPER');
        },
        addFeatureRef(feat = null) {
            const vm = this;
            const opt = {
                fill: 'rgba(0,0,255,.15)',
                stroke: 'blue',
                strokeWidth: 5,
                width: 150,
                height: 150,
                // left: 50,
                // top: 50,
            }

            if (feat) {
                opt.top = feat.y;
                opt.left = feat.x;
                opt.width = feat.width;
                opt.height = feat.height;
            }

            return this.imageEditor.invoke('addShape', 'rect', opt).then((props) => {
                // console.log('addFeatureRef: ', props);

                const obj = {
                    id: props.id,
                };

                if (feat) {
                    obj.content = feat.content;
                    obj.type = feat.type;
                }

                vm.setFeatureRef(obj);
                vm.updateInfo();
                vm.discardSelection();
            });
        },
        removeFeature(feature) {
            const vm = this;
            this.imageEditor.invoke('removeObject', feature.id)
                .then(() => {
                    vm.removeFeatureRef(feature);
                })
                .catch(() => {
                    vm.removeFeatureRef(feature);
                })
            ;
        },
        toDataURL(opt) {
            return this.imageEditor.invoke('toDataURL', opt);
        },
        rotate(angle) {
            const vm = this;
            this.imageEditor.invoke('rotate', angle)
                .then(() => {
                    vm.updateActiveImgSrc();
                    vm.updateInfo();
                })
            ;
        },
        rotateClockwise() {
            this.rotate(-90);
        },
        rotateCounterClockwise() {
            this.rotate(90);
        },
        discardSelection() {
            this.imageEditor.invoke('discardSelection');
            this.activeFeatureId = null;
            this.updateInfo();
        },
        startDrawingMode(mode) {
            this.imageEditor.invoke('startDrawingMode', mode);
            this.updateInfo();
        },
        stopDrawingMode() {
            this.imageEditor.invoke('stopDrawingMode');
            this.updateInfo();
        },
        clearObjects() {
            const vm = this;
            return this.imageEditor.invoke('clearObjects')
                .then(() => {
                    // console.log('objects cleared');
                    vm.updateInfo();
                });
        },
        clearRedoStack() {
            this.imageEditor.invoke('clearRedoStack');
            this.updateInfo();
        },
        clearUndoStack() {
            this.imageEditor.invoke('clearUndoStack');
            this.updateInfo();
        },
        getObjectProperties(id, keys) {
            return this.imageEditor.invoke('getObjectProperties', id, keys);
        },
        loadImage(src, id) {
            const vm = this;

            if (this.activeImageId === id) {
                return;
            }

            this.updateFeatures();

            return this.resetAll().then(() => {
                this.imageEditor.invoke('loadImageFromURL', src, id)
                    .then(result => {
                        if (vm.hasFeatures(id)) {
                            const features = vm.getFeatures(id);
                            vm.initFeatures(features).then(feat => {
                                vm.updateInfo();
                                resizeEditor();
                            });
                        } else {
                            vm.updateInfo();
                            resizeEditor();
                        }
                    });
            });
        },
        updateFeatures() {
            if (this.hasFeatureRef) {
                this.setFeatures(this.activeImageId, this.features);
            } else {
                this.removeFeatures(this.activeImageId);
            }
        },
        updateActiveImgSrc() {
            const vm = this;

            // remove features
            this.updateFeatures();

            return this.resetAll().then(() => {
                // update img src
                vm.updateImgSrc(vm.activeImageId, vm.toDataURL());

                // add features
                if (vm.hasFeatures(vm.activeImageId)) {
                    const features = vm.getFeatures(vm.activeImageId);
                    return vm.initFeatures(features);
                }
            });
        },
        initFeatures(features, idx = 0) {
            if (features.length === idx) {
                return;
            }

            const vm = this;
            return this.addFeatureRef(features[idx]).then(() => {
                vm.initFeatures(features, idx + 1);
            });
        },


        // FeaturePool
        hasFeatures(imgId) {
            return has.call(this.featurePool, imgId);
        },
        setFeatures(imgId, features) {
            const vm = this;
            const featurePool = [];
            features.forEach((feat) => {

                let obj = vm.getObjectProperties(feat.id, [
                    'top',
                    'left',
                    'width',
                    'height',
                ]);

                featurePool.push({
                    x: parseInt(obj.left),
                    y: parseInt(obj.top),
                    width: parseInt(obj.width),
                    height: parseInt(obj.height),
                    type: feat.type || '',
                    content: feat.content || '',
                });
            });
            this.$set(this.featurePool, imgId, featurePool);
        },
        removeFeatures(imgId) {
            this.$delete(this.featurePool, imgId);
        },
        getFeatures(imgId) {
            return this.featurePool[imgId];
        },

        // Features Ref
        setFeatureRef(feature) {
            this.$set(this.featureRefPool, feature.id, feature);
        },
        removeFeatureRef(feature) {
            this.$delete(this.featureRefPool, feature.id);
        },

        // Others
        isActiveImage(imgId) {
            return this.getActiveImageId === imgId;
        },
    },

}
