import {DEFAULT_DELAY_REQUEST_ACTIONS} from '../../config';
import {dataUrlToBlob, has, minDelay} from '../../utilities/helpers';
import {DocApi} from '../services/doc_api';
import Vue from 'vue';

export default {

    /*
    |---------------------------------------------------------------------------
    | Composition
    |---------------------------------------------------------------------------
    */

    // mixins: [],

    // extends: {},

    /*
    |---------------------------------------------------------------------------
    | Assets
    |---------------------------------------------------------------------------
    */

    // directives: {},

    // filters: {},

    // components: {},

    /*
    |---------------------------------------------------------------------------
    | Lifecycle Hooks
    |---------------------------------------------------------------------------
    */

    // created() {},

    // mounted() {},

    /*
    |---------------------------------------------------------------------------
    | Data
    |---------------------------------------------------------------------------
    */

    // props: {},

    data() {
        return {
            docImagesRef: {},

            fetchImagesFromDocumentActionRequest: false,
            saveImagesForDocumentActionRequest: false,
        };
    },

    computed: {
        docImages() { return Object.values(this.docImagesRef); },

        isFetchImagesFromDocumentActionRequesting() {
            return this.fetchImagesFromDocumentActionRequest;
        },
        isSaveImagesForDocumentActionRequesting() {
            return this.saveImagesForDocumentActionRequest;
        },
    },

    // watch: {},

    methods: {
        setDocImages(images) {
            const vm = this;
            const imgs = {};
            images.forEach((img) => {
                img.src = vm.imgAsset(img.name) + `?v=${img.updated_at}`;
                img.srcChanged = false;
                imgs[img._id] = img;
                if (has.call(img, 'imageFeatures') && Array.isArray(img.imageFeatures)) {
                    const featurePool = [];
                    img.imageFeatures.forEach((feat) => {
                        featurePool.push({
                            x: parseInt(feat.x),
                            y: parseInt(feat.y),
                            width: parseInt(feat.width),
                            height: parseInt(feat.height),
                            type: feat.type || '',
                            content: feat.content || '',
                        });
                    });
                    if (featurePool.length) {
                        vm.$set(vm.featurePool, img._id, featurePool);
                    }
                }
            });
            Vue.set(this, 'docImagesRef', imgs);
        },
        hasDocImage(id) {
            return has.call(this.docImagesRef, id);
        },
        updateImgSrc(id, src) {
            this.docImagesRef[id].src = src;
            this.docImagesRef[id].srcChanged = true;
        },
        getDocImage(docImg) {
            return this.docImages[docImg];
        },
        fetchImagesFromDocumentAction(docId) {
            const vm = this;
            this.fetchImagesFromDocumentActionRequest = true;
            return DocApi.fetchImagesFromDocument(docId)
                .then(({data}) => {
                    vm.setDocImages(data);
                    vm.fetchImagesFromDocumentActionRequest = false;
                })
                .catch(({response}) => {
                    vm.fetchImagesFromDocumentActionRequest = false;
                })
            ;
        },
        saveImagesForDocumentAction() {
            const vm = this;
            const now = Date.now();

            const docId = this.docId;

            this.updateFeatures();

            const formData = new FormData();

            const images = this.docImages;

            images.forEach((img) => {
                const imgId = img._id;

                if (vm.hasFeatures(imgId)) {
                    let idx = 0;
                    vm.getFeatures(imgId).forEach((feat) => {
                        formData.append(`features[${imgId}][${idx}][x]`, feat.x);
                        formData.append(`features[${imgId}][${idx}][y]`, feat.y);
                        formData.append(`features[${imgId}][${idx}][width]`, feat.width);
                        formData.append(`features[${imgId}][${idx}][height]`, feat.height);
                        formData.append(`features[${imgId}][${idx}][type]`, feat.type);
                        formData.append(`features[${imgId}][${idx}][content]`, feat.content);
                        // formData.append(`features[${imgId}][${idx}][]`, feat.);
                        idx++;
                    });
                } else {
                    // remove features
                    formData.append(`features[${imgId}]`, '');
                }

                if (img.srcChanged) {
                    formData.append(`images[${imgId}]`, dataUrlToBlob(img.src, img.name));
                }
            });

            this.saveImagesForDocumentActionRequest = true;
            return DocApi.saveImagesForDocument(docId, formData)
                .then(({data}) => {
                    minDelay(now, DEFAULT_DELAY_REQUEST_ACTIONS, () => {
                        vm.saveImagesForDocumentActionRequest = false;
                        vm.notifier.alert(vm.alertRef).success(data.message);
                    });

                })
                .catch(({response}) => {
                    minDelay(now, DEFAULT_DELAY_REQUEST_ACTIONS, () => {
                        vm.saveImagesForDocumentActionRequest = false;
                        vm.notifier.alert(vm.alertRef).danger(response.data.message);
                    });
                })
                ;
        }
    },

}
