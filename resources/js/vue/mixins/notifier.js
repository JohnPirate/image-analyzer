import Notifier from '../utilities/notyfier';

export default {

    created() {
        this.__notifier = new Notifier(this.$root);
    },

    data() {
        return {
            __notifier: null,
        };
    },

    computed: {
        notifier() {
            return this.__notifier;
        }
    }
}