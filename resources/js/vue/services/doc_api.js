import axios from 'axios';

export const DocApi = {
    /**
     * @param {String} docId
     * @returns {Promise<AxiosResponse<any>>}
     */
    fetchImagesFromDocument(docId) {
        const url = `/api/v1/documents/${docId}/images`;
        return axios.get(url);
    },

    /**
     * @param {String} docId
     * @param {FormData} formData
     * @returns {Promise<AxiosResponse<any>>}
     */
    saveImagesForDocument(docId, formData) {
        const url = `/api/v1/documents/${docId}/images`;
        return axios.post(url, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
    },
}
