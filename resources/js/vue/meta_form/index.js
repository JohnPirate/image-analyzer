import Vue from 'vue';
import MetaForm from './components/meta_form';

const elementId = 'meta-form';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            MetaForm,
        },

        data() {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            const formRef = JSON.parse(environmentsData.form) || {};
            return {
                // data properties
                docId: environmentsData.docId,
                formRef,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    docId: this.docId,
                    formRef: this.formRef,
                }
            });
        }
    });
});
