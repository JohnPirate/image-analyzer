import Vue from 'vue';
import ImageEditor from './components/image_editor';

const elementId = 'image-editor';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            ImageEditor,
        },

        data() {
            const envData = document.querySelector(`#${elementId}`).dataset;
            return {
                // data properties
                docId: envData.docId,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    docId: this.docId,
                }
            });
        }
    });
});
