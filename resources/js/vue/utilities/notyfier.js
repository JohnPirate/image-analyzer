import Alert from './alert';
import {has} from '../../utilities/helpers';

/**
 * Notifier Class
 * @type {class}
 */
export default class Notifier {

    /**
     * Alert constructor
     * @param {object} hub
     * @param {String} ref
     */
    constructor(hub, ref = 'gl') {
        this.hub = hub;
        this.ref = ref;

        this._alert = {};
        this._alert[ref] = new Alert(ref, hub);
    }

    /**
     * @param {String|null} ref
     * @return {Alert}
     */
    alert(ref = null) {
        if (ref) {
            if (!has.call(this._alert, ref)) {
                this._alert[ref] = new Alert(ref, this.hub);
            }
            return this._alert[ref];
        }
        return this._alert[this.ref];
    }
}