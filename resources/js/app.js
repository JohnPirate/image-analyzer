import './bootstrap';

/**
 * @type {(v: PropertyKey) => boolean}
 */
window.has = Object.prototype.hasOwnProperty;

/**
 * @param {number}  bytes
 * @param {boolean} si
 * @returns {string}
 */
window.humanFileSize = (bytes, si = true) => {
    let thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }

    const units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];

    let u = -1;

    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);

    return bytes.toFixed(1) + ' ' + units[u];
};

/**
 * @param value
 * @param decimals
 * @returns {number}
 */
window.round = (value, decimals) => {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
};

/**
 * Set an EventListener to a collection of elements or only one element
 *
 * @param {HTMLCollection|NodeList|Array|Object} elements
 * @param {String}                               type
 * @param {Function}                             cb
 */
window.setEventListener = (elements, type, cb) => {
    if (elements instanceof HTMLCollection || elements.length > 0) {
        for (let idx = 0; idx < elements.length; idx++) {
            elements[idx].addEventListener(type, cb);
        }
    } else {
        elements.addEventListener(type, cb);
    }
};

/**
 * Remove an EventListener from a collection of elements or only one element
 *
 * @param {HTMLCollection|NodeList|Array|Object} elements
 * @param {String}                               type
 * @param {Function}                             cb
 */
window.unsetEventListener = (elements, type, cb) => {
    if (elements instanceof HTMLCollection || elements.length > 0) {
        for (let idx = 0; idx < elements.length; idx++) {
            elements[idx].removeEventListener(type, cb);
        }
    } else {
        elements.removeEventListener(type, cb);
    }
};

window.confirmFormSubmitAction = ({e = null, msg = ''}) => {
    e.preventDefault();
    if (!window.confirm(msg)) {
        return false;
    }
    e.target.submit();
    return true;
};

window.deleteDocumentModal = (e) => {
    return confirmFormSubmitAction({e, msg: 'Do you really want to delete this document?'});
};

window.deleteImageModal = (e) => {
    return confirmFormSubmitAction({e, msg: 'Do you really want to delete this image?'});
};
