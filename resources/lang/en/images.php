<?php

return [
    'types' => [
        'frontside' => 'Frontside',
        'backside' => 'Backside',
        'leftside' => 'Leftside',
        'rightside' => 'Rightside',
        'top' => 'Top',
        'bottom' => 'Bottom',
        'nutritionfacs' => 'Nutrition facts',
        'others' => 'Others',
    ],
];
