<?php

return [
    'images' => [
        'types' => [
            'frontside',
            'backside',
            'leftside',
            'rightside',
            'top',
            'bottom',
            'nutritionfacs',
            'others',
        ],
    ],
];
