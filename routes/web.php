<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');


Route::prefix('/documents')->name('documents.')->group(function () {

    Route::get('/', 'DocumentController@overview')->name('overview');
    Route::post('/', 'DocumentController@create')->name('create');
    Route::get('/{documentId}', 'DocumentController@show')->name('show');
    Route::put('/{documentId}', 'DocumentController@update')->name('update');
    Route::delete('/{documentId}', 'DocumentController@delete')->name('delete');
    Route::get('/{documentId}/edit', 'DocumentController@edit')->name('edit');

    Route::get('/{documentId}/editor', 'ImageEditorController@index')->name('editor');
});

Route::prefix('/images')->name('images.')->group(function () {

    Route::post('/', 'ImageController@upload')->name('upload');
    Route::patch('/{imageId}', 'ImageController@patch')->name('patch');
    Route::delete('/{imageId}', 'ImageController@delete')->name('delete');

});

Route::prefix('/api/v1')->name('api_v1.')->group(function () {

    Route::prefix('/documents')->name('documents.')->group(function () {

        Route::get('/{documentId}/images', 'ImageEditorController@getImages')->name('get_images');
        Route::post('/{documentId}/images', 'ImageEditorController@saveImages')->name('save_images');

    });

});
