const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/vue/meta_form/index.js', 'public/js/meta_form.js')
    .js('resources/js/vue/image_editor/index.js', 'public/js/image_editor.js')
    .extract(['bootstrap', 'jquery', 'axios', 'lodash', 'vue'])
    .sass('resources/sass/vendor.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
;
mix.version();
mix.disableNotifications();
