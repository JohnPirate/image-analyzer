<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Document
 * @package App
 *
 * @property-read string $id
 * @property array $meta
 * @property string $userkey
 * @property \Illuminate\Support\Collection $imageFiles
 * @property string $created_at
 * @property string $updated_at
 */
class Document extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userkey', 'meta',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function imageFiles()
    {
        return $this->embedsMany(ImageFile::class);
    }

    public function hasImageFiles()
    {
        return $this->imageFiles() ? true : false;
    }
}
