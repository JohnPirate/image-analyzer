<?php

namespace App\Http\Controllers;

use App\Handler\ApiHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return \App\Handler\ApiHandler
     */
    protected function apiHandler(): ApiHandler
    {
        return app(ApiHandler::class);
    }

    /**
     * @return string|null
     */
    protected function getUserKey(): ?string
    {
        return $_SERVER['PHP_AUTH_USER'] ?? null;
    }

    protected function handleException(\Exception $ex)
    {
        $exClass = get_class($ex);
        $status = 500;

        session()->flash('alert_msg', $ex->getMessage());
        session()->flash('alert_type', 'danger');

        if (in_array($exClass, [NotFoundResourceException::class], false)) {
            $status = 404;
            session()->flash('alert_type', 'warning');

        } elseif (in_array($exClass, [LogicException::class], false)) {
            $status = 400;
            session()->flash('alert_type', 'warning');

        } elseif (in_array($exClass, [AuthorizationException::class], false)) {
            $status = 403;
        } elseif (in_array($exClass, [\RuntimeException::class], false)) {

        } else {
            session()->flash('alert_msg', 'Internal Error');
        }

        Log::error($ex->getMessage(), [
            'exception_type' => $exClass,
            'response_status' => $status,
        ]);

        return $status;
    }

    /**
     * @param \Exception $ex
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleApiException(\Exception $ex)
    {
        $exClass = get_class($ex);
        $res = [
            'message' => $ex->getMessage(),
        ];
        $status = 500;
        if (in_array($exClass, [NotFoundResourceException::class], false)) {
            $status = 404;
        } elseif (in_array($exClass, [LogicException::class], false)) {
            $status = 400;
        } elseif (in_array($exClass, [AuthorizationException::class], false)) {
            $status = 403;
        } else {
            $res['message'] = 'Internal Error';
        }

        Log::error($ex->getMessage(), [
            'exception_type' => $exClass,
            'response_status' => $status,
        ]);

        return response()->json($res, $status);
    }
}
