<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImageDocumentRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class ImageController
 * @package App\Http\Controllers
 */
class ImageController extends Controller
{
    /**
     * @param \App\Http\Requests\CreateImageDocumentRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function upload(CreateImageDocumentRequest $request)
    {
        try {

            $userkey = $this->getUserKey();

            $documentId = $request->request->get('document_id');
            $doc = $this->apiHandler()->getDocumentUsingId($documentId);

            /** @var \Illuminate\Http\UploadedFile[] $images */
            $images = $request->file('images');
            if (count($images) > 0) {
                $type = $request->request->get('type');
                foreach ($images as $image) {
                    if (!$image->isValid()) {
                        throw new \RuntimeException('File is invalid.');
                    }

                    $fileExtension = $image->extension();
                    $fileName = Str::uuid() . '.' . $fileExtension;
                    $image->storeAs('public/images', $fileName);

                    $doc->imageFiles()->create([
                        'userkey' => $userkey,
                        'name' => $fileName,
                        'type' => $type,
                    ]);
                }
            }

            session()->flash('alert_type', 'success');
            session()->flash('alert_msg', 'ImageFile successfully uploaded.');

            return redirect(route('documents.show', compact('documentId')));

        } catch (\Exception $ex) {
            $this->handleException($ex);
            return redirect(route('documents.overview'));
        }
    }

    /**
     * @param string $imageId
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($imageId)
    {
        try {

            $doc = $this->apiHandler()->deleteImageUsingId($imageId);

            session()->flash('alert_type', 'success');
            session()->flash('alert_msg', 'ImageFile successfully deleted.');

            return redirect(route('documents.show', ['documentId' => $doc->id]));

        } catch (\Exception $ex) {
            $this->handleException($ex);
            return redirect(route('documents.overview'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $imageId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patch(Request $request, $imageId)
    {
        try {

            $this->apiHandler()->updateImageUsingId($imageId, $request->json()->all());

            return response()->json([
                'message' => 'update successful',
            ]);

        } catch (\Exception $ex) {
            dump($ex);
            return $this->handleApiException($ex);
        }
    }
}
