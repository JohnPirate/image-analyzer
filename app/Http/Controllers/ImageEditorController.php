<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class ImageEditorController
 * @package App\Http\Controllers
 */
class ImageEditorController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $documentId
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $request, $documentId)
    {
        try {

            $doc = $this->apiHandler()->getDocumentUsingId($documentId);
            return view('documents.editor', compact('doc'));

        } catch (\Exception $ex) {
            $this->handleException($ex);
            return redirect(route('documents.overview'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $documentId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getImages(Request $request, $documentId)
    {
        try {

            $doc = $this->apiHandler()->getDocumentUsingId($documentId);
            return response()->json($doc->imageFiles);

        } catch (\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    public function saveImages(Request $request, $documentId)
    {
        try {

            // if has files -> save them first
            if ($request->files->count() > 0) {
                $images = $request->file('images');
                foreach ($images as $id => $image) {
                    $fileName = $image->getClientOriginalName();
                    $image->storeAs('public/images', $fileName);
                    $docImg = $this->apiHandler()->getImageFileUsingId($id);
                    $docImg->updated_at = date('Y-m-d H:i:s');
                    $docImg->save();
                }
            }

            // save features
            $this->apiHandler()->saveFeatures($request->request->get('features', []));
            return response()->json(['message' => 'Successfully updated.']);

        } catch (\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }
}
