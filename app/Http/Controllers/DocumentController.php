<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Requests\CreateImageDocumentRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class DocumentController
 * @package App\Http\Controllers
 */
class DocumentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function overview()
    {
        $documents = Document::orderBy('created_at', 'desc')->get();
        return view('documents.overview', compact('documents'));
    }

    /**
     * @param \App\Http\Requests\CreateImageDocumentRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(CreateImageDocumentRequest $request): RedirectResponse
    {
        try {

            $userkey = $this->getUserKey();

            $doc = new Document;
            $doc->fill([
                'userkey' => $userkey,
            ]);
            $doc->save();

            /** @var \Illuminate\Http\UploadedFile[] $images */
            $images = $request->file('images');
            if (count($images) > 0) {
                $type = $request->request->get('type');
                foreach ($images as $image) {
                    if (!$image->isValid()) {
                        throw new \RuntimeException('File is invalid.');
                    }

                    $fileExtension = $image->extension();
                    $fileName = Str::uuid() . '.' . $fileExtension;
                    $image->storeAs('public/images', $fileName);

                    $doc->imageFiles()->create([
                        'userkey' => $userkey,
                        'name' => $fileName,
                        'type' => $type,
                    ]);
                }
            }

            return redirect(route('documents.show', ['documentId' => $doc->id]));

        } catch (\Exception $ex) {
            $this->handleException($ex);
            return redirect(route('index'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $documentId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show(Request $request, $documentId)
    {
        try {

            $doc = $this->apiHandler()->getDocumentUsingId($documentId);
            return view('documents.show', compact('doc'));

        } catch (\Exception $ex) {
            $this->handleException($ex);
            return redirect(route('documents.overview'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $documentId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $documentId)
    {
        try {

            $doc = $this->apiHandler()->getDocumentUsingId($documentId);
            return view('documents.edit', compact('doc'));

        } catch (\Exception $ex) {
            $this->handleException($ex);
            return redirect(route('documents.overview'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $documentId
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $documentId)
    {
        try {

            $meta = $request->request->get('meta');

            $this->apiHandler()->updateDocumentUsingId($documentId, $meta);

            session()->flash('alert_type', 'success');
            session()->flash('alert_msg', 'Document successfully updated.');

        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return redirect(route('documents.show', compact('documentId')));
    }

    /**
     * @param string $documentId
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($documentId)
    {
        try {

            $doc = $this->apiHandler()->deleteDocumentUsingId($documentId);

            session()->flash('alert_type', 'success');
            session()->flash('alert_msg', 'Document successfully deleted.');

        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return redirect(route('documents.overview'));
    }
}
