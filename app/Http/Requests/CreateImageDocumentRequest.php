<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateImageDocumentRequest
 * @package App\Http\Requests
 */
class CreateImageDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $types = implode(',', config('common.images.types', []));
        return [
            'images' => 'required|max:8192', // max in kb
            'type' => 'required|in:'.$types,
        ];
    }
}
