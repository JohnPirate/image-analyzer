<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class ImageFile
 * @package App
 *
 * @property-read string $id
 * @property string $userkey
 * @property string $name
 * @property string $type
 * @property \Illuminate\Support\Collection $imageFeatures
 * @property-read \App\Document $document
 * @property string $created_at
 * @property string $updated_at
 */
class ImageFile extends Eloquent
{
    protected $fillable = [
        'userkey',
        'name',
        'type',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    /**
     * @return bool
     */
    public function deleteRelatedFile(): bool
    {
        return Storage::delete('public/images/'.$this->name);
    }

    public function imageFeatures()
    {
        return $this->embedsMany(ImageFeatures::class);
    }

    public function hasImageFeatures()
    {
        return $this->imageFeatures() ? true : false;
    }
}
