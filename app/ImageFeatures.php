<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class ImageFeatures
 * @package App
 *
 * @property-read string $id
 * @property int $y
 * @property int $x
 * @property int $height
 * @property int $width
 * @property string $type
 * @property string $content
 * @property-read \App\ImageFile $image
 * @property string $created_at
 * @property string $updated_at
 */
class ImageFeatures extends Eloquent
{
    protected $fillable = [
        'x',
        'y',
        'width',
        'height',
        'type',
        'content',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];


}
