<?php

namespace App\Handler;

use App\Document;
use App\ImageFile;
use MongoDB\BSON\ObjectId;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class ApiHandler
 * @package App\Handler
 */
class ApiHandler
{
    /**
     * @param string $documentId
     *
     * @return \App\Document
     */
    public function getDocumentUsingId(?string $documentId): Document
    {
        $doc = Document::find($documentId);
        if (!$doc) {
            throw new NotFoundResourceException('Document not found for ID '.$documentId);
        }
        return $doc;
    }

    /**
     * @param string|null $imageId
     *
     * @return \App\ImageFile
     */
    public function getImageFileUsingId(?string $imageId): ImageFile
    {
        $doc = Document::raw(function($collection) use ($imageId) {
            return $collection->findOne([
                'imageFiles._id' => new ObjectId($imageId),
            ]);
        });
        if (!$doc) {
            throw new NotFoundResourceException('ImageFile not found for ID '.$imageId);
        }
        return $doc->imageFiles->filter(function (ImageFile $imageFile) use ($imageId) {
            return $imageFile->id === $imageId;
        })->first();
    }

    /**
     * @param string|null $documentId
     *
     * @return bool|null
     * @throws \Exception
     */
    public function deleteDocumentUsingId(?string $documentId): ?bool
    {
        $doc = $this->getDocumentUsingId($documentId);
        foreach ($doc->imageFiles as $imageFile) {
            $imageFile->deleteRelatedFile();
        }
        return $doc->delete();
    }

    /**
     * @param string|null $imageId
     *
     * @return \App\Document
     * @throws \Exception
     */
    public function deleteImageUsingId(?string $imageId): Document
    {
        $imageFile = $this->getImageFileUsingId($imageId);
        $imageFile->deleteRelatedFile();
        $imageFile->delete();
        return $imageFile->document;
    }

    /**
     * @param string     $documentId
     * @param array|null $data
     *
     * @return void
     * @throws \Exception
     */
    public function updateDocumentUsingId(string $documentId, ?array $data): void
    {
        $doc = $this->getDocumentUsingId($documentId);

        $doc->meta = null;

        if ($data) {
            // Convert data from request to key/val array
            $keyVal = [];
            foreach ($data as $datum) {
                $keyVal[$datum['key']] = $datum['val'];
            }

            $doc->meta = $keyVal;
        }

        $doc->save();
    }

    /**
     * @param string $imageId
     * @param array  $data
     *
     * @return void
     */
    public function updateImageUsingId(string $imageId, array $data): void
    {
        $img = $this->getImageFileUsingId($imageId);

        if (isset($data['type'])) {
            $img->type = $data['type'];
        }

        if ($img->getChanges() > 0) {
            $img->save();
        }
    }

    public function saveFeatures(array $images)
    {
        foreach ($images as $imageId => $features) {

            $img = $this->getImageFileUsingId($imageId);

            $img->imageFeatures()->each(function ($feat) {
                $feat->delete();
            });

            if (!$features) {
                continue;
            }

            foreach ($features as $feature) {
                $img->imageFeatures()->create($feature);
            }

            $img->save();
        }
    }
}
